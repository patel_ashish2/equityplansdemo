//
//  CompanyViewModel.swift
//  EquityPlansDemo
//
//  Created by Ashish Patel on 2019/04/07.
//  Copyright © 2019. All rights reserved.
//

import Foundation

class CompanyViewModel  {

    let appServerClient: AppServerClient
    var onShowError: ((_ alert: SingleButtonAlert) -> Void)?
    let showLoadingHud: Bindable = Bindable(false)
    let name = Bindable(String.init())
    var symbol = Bindable(String.init())
    var value = Bindable(String.init())
    
    init(appServerClient: AppServerClient = AppServerClient()) {
        self.appServerClient = appServerClient
    }
    // current company details
    func getCurrentCompanyData(){
        showLoadingHud.value = true
        appServerClient.getCompanyData{ [weak self] result in
            guard let strongSelf = self  else { return }
            strongSelf.showLoadingHud.value = false
            switch result {
            case .success(let data):
                strongSelf.name.value = data.name
                strongSelf.symbol.value = data.symbol
                strongSelf.value.value = String(data.value)
            case .failure(let error):
                let okAlert = SingleButtonAlert(
                    title: error?.getErrorMessage() ?? "Could not connect to server. Check your network and try again later.",
                    message: "Failed to update information.",
                    action: AlertAction(buttonTitle: "OK", handler: { print("Ok pressed!") })
                )
                self?.onShowError?(okAlert)
            }
        }
    }
}

// failur message
fileprivate extension AppServerClient.CompanyFailureReason {
    func getErrorMessage() -> String? {
        switch self {
        case .notFound:
            return "Failed to retrive company info. Please try again."
        }
    }
}
