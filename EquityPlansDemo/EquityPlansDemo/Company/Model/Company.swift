//
//  Company.swift
//  EquityPlansDemo
//
//  Created by Ashish Patel on 2019/04/07.
//  Copyright © 2019. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Company {
    let value:Float
    let symbol:String
    let name:String
}

extension Company{
    init?(json: JSON) {
        guard
            let value = json["value"].float,
            let symbol = json["symbol"].string,
        let name = json["name"].string
        else {
            return nil
        }
        self.value = value
        self.symbol = symbol
        self.name = name

    }
}
