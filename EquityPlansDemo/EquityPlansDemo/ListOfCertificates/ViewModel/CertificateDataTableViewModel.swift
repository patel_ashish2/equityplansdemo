//
//  CertificateDataTableViewModel.swift
//  EquityPlansDemo
//
//  Created by Ashish Patel on 2019/04/07.
//  Copyright © 2019. All rights reserved.
//

import Foundation

class CertificateDataTableViewModel {
    enum CertificateTableViewCellType {
        case normal(cellValue:(CertificateModel))
        case error(message: String)
        case empty
 }
    fileprivate let formatter = DateFormatter()
    var onShowError: ((_ alert: SingleButtonAlert) -> Void)?
    let showLoadingHud: Bindable = Bindable(false)
    
    let aryCertificates = Bindable([CertificateTableViewCellType]())
    let navTitle:Bindable = Bindable(String.init())
    let appServerClient: AppServerClient
    
    init(appServerClient: AppServerClient = AppServerClient()) {
        self.appServerClient = appServerClient
    }
    func getCertificateData() {
        showLoadingHud.value = true
        appServerClient.getListOfCertificates{ [weak self] result in
           self?.showLoadingHud.value = false
            switch result {
            case .success(let data):
                //self?.navTitle.value = data.cityName
                guard  data.count > 0, let cellData =  self?.cells(from: data) else {
                    self?.aryCertificates.value = [.empty]
                    return
                }
                self?.aryCertificates.value = cellData.compactMap{.normal(cellValue: $0) }
               
            case .failure(let error):
                self?.aryCertificates.value = [.error(message: error?.getErrorMessage() ?? "Loading failed, check network connection")]
            }
        }
    }
    
    func postCertificateDataForSellingWith(parmas:[String:AnyObject], compliton: @escaping(_ result: Bool) -> Void) {
        showLoadingHud.value = true
        appServerClient.postListOfSellingCertificates(params: parmas, completion: { [weak self] result in
            self?.showLoadingHud.value = false
            switch result {
            case .success( _): break
                //self?.navTitle.value = data.cityName
            case .failure( _): break
            }
            compliton(true)
        }) }

    


    private func cells(from certificates: [Certificate])-> [(CertificateModel)]{
    
    //There's probably a better way to write this.
    
    func dateTimestampFromInterval(date: String)-> String {
       let date =  Date(timeIntervalSince1970: TimeInterval(Int(date)!)/1000)
        formatter.dateFormat = "yyyy-MM-dd HH:mm"
        return formatter.string(from: date as Date)
    }
    
        func dayTimestampFromDateTimstamp(timestamp: String)-> String {
        //print(timestamp)
            var extractedTimestamp = timestamp
        extractedTimestamp = extractedTimestamp.replacingOccurrences(of: "/Date(", with: "")
        extractedTimestamp = extractedTimestamp.replacingOccurrences(of: ")/", with: "")
        extractedTimestamp = dateTimestampFromInterval(date: extractedTimestamp)
        return extractedTimestamp
    }
    
    
    let certificateModels = certificates.compactMap { (data) -> CertificateModel? in
        let string = dayTimestampFromDateTimstamp(timestamp: data.issuedDate ?? "")
        data.issuedDate = string
        return certificateModel(from: data)
    }
    
    //Combine models into an Array of tuples
    return certificateModels
}

 private func certificateModel(from certificate: Certificate)-> CertificateModel {
    return CertificateModel(
        certificateId: "\(certificate.certificateId)",
        numberOfShares: "\(certificate.numberOfShares ?? "")",
        issuedDate: "\(certificate.issuedDate ?? "")",
        numberOfShareForSelling: "0")}}

private extension AppServerClient.CertificatesFailureReason {
    func getErrorMessage() -> String? {
        switch self {
        case .notFound:
            return "Failed to retrive list of certificates. Please try again."
        }
    }
}

