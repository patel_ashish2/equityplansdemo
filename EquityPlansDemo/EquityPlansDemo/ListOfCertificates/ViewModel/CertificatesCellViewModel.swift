//
//  CertificatesCellViewModel.swift
//  EquityPlansDemo
//
//  Created by Ashish Patel on 2019/04/07.
//  Copyright © 2019. All rights reserved.
//

import Foundation


protocol CertificatesCellViewModel {
    var certificateNoStr:String{ get }
    var noOfSharesStr:String { get }
    var issueDateStr:String { get }
    var noOfSharesForSellingStr:String { get }
}

extension Certificate: CertificatesCellViewModel{
    var noOfSharesForSellingStr: String {
        return self.numberOfSharesForSelling ?? ""
    }
    
    var certificateNoStr: String {
        return String(self.certificateId)
    }
    
    var noOfSharesStr: String {
        return self.numberOfShares ?? ""
    }
    
    var issueDateStr: String {
        return self.issuedDate ?? ""
    }
    
}
