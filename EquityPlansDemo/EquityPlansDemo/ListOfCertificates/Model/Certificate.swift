//
//  Certificate.swift
//  EquityPlansDemo
//
//  Created by Ashish Patel on 2019/04/07.
//  Copyright © 2019. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper
import ObjectMapperAdditions

class Certificate : Mappable {
    @objc private(set) dynamic var certificateId = 0
    var numberOfShares: String? = nil
    var issuedDate: String? = nil
    var numberOfSharesForSelling: String? = nil
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        certificateId <- (map["certificateId"], IntTransform())
        issuedDate <- (map["issuedDate"], StringTransform())
        numberOfShares <- (map["numberOfShares"], StringTransform())
    }

}


