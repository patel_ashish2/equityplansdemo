//
//  WeeklyTableViewCell.swift
//  EquityPlansDemo
//
//  Created by Ashish Patel on 2019/04/07.
//  Copyright © 2019. All rights reserved.
//

import UIKit

class CertificateTableViewCell: UITableViewCell {
    @IBOutlet weak var lblCertificateId: UILabel!
    @IBOutlet weak var lblNumberOfCertificate: UILabel!
    @IBOutlet weak var lblNumberOfCertificateForSelling: UILabel!
    @IBOutlet weak var lblIssueDate: UILabel!

    func configureCell(model:CertificateModel?){
        self.lblCertificateId.text = "Certifice \(model?.certificateId ?? "")"
        self.lblNumberOfCertificate.text = "Number of shares: \(model?.numberOfShares ?? "")"
        self.lblNumberOfCertificateForSelling.text = "Number of shares for selling: \(model?.numberOfShareForSelling ?? "")"
        self.lblIssueDate.text = model?.issuedDate
    }
    var viewModel: CertificatesCellViewModel? {
        didSet {
            bindViewModel()
        }
    }

    private func bindViewModel() {
        if let no = viewModel?.certificateNoStr{
            lblCertificateId.text = no
        }
        if let share = viewModel?.noOfSharesStr {
           lblNumberOfCertificate.text = share
        }
        if let date = viewModel?.issueDateStr {
            lblIssueDate.text = date
        }
        if let selling = viewModel?.noOfSharesForSellingStr {
            lblNumberOfCertificateForSelling.text = selling
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
