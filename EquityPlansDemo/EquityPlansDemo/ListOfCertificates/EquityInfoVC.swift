//
//  EquityInfoVC.swift
//  EquityPlansDemo
//
//  Created by Ashish Patel on 2019/04/07.
//  Copyright © 2019. All rights reserved.
//

import UIKit
import CoreLocation

struct CertificateModel {
    let certificateId: String
    let numberOfShares: String
    let issuedDate: String
    var numberOfShareForSelling: String
}

class EquityInfoVC: UIViewController {
     var lblCurrentRate: UILabel!

    @IBOutlet weak var tableView: UITableView!
    let companyViewModel: CompanyViewModel = CompanyViewModel()
    let certificateViewModel: CertificateDataTableViewModel = CertificateDataTableViewModel()
    
    
    // Viewcontroller lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        bindCertificateViewModel()
        bindCompanyViewModel()
        tableView.tableFooterView = UIView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getEquityData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    //get equity data
    @objc func getEquityData() {
        let headerView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        headerView.backgroundColor = .clear
        lblCurrentRate = UILabel.init(frame: CGRect(x: 0, y: 0, width: headerView.frame.size.width, height: 50))
        lblCurrentRate.text = "Current Share Price:"
        lblCurrentRate.textAlignment = .center
        headerView.addSubview(lblCurrentRate)
        self.tableView.tableHeaderView = headerView
        DispatchQueue.once {
            certificateViewModel.getCertificateData()
            companyViewModel.getCurrentCompanyData()
        }
        startTimerForResendingCode()
    }
    func startTimerForResendingCode() {
        _ = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(getCurrentCompanyData), userInfo: nil, repeats: true)

    }
    
    @objc func getCurrentCompanyData() {
        DispatchQueue.global(qos: .background).async {
            self.companyViewModel.getCurrentCompanyData()
        }
    }

    // bind certificate view model
    func bindCertificateViewModel() {
        certificateViewModel.aryCertificates.bindAndFire() { [weak self] _ in
            self?.tableView?.reloadData()
        }
        certificateViewModel.onShowError = { [weak self] alert in
           self?.presentSingleButtonDialog(alert: alert)
        }
        certificateViewModel.showLoadingHud.bind() { visible in
            print(visible)
            visible ? SharedClass.sharedInstance.showLoader(strTitle: "") : SharedClass.sharedInstance.dismissLoader()
        }
    }

    // bind  company view model
    private func bindCompanyViewModel(){
        companyViewModel.name.bind{ [weak self] name in
            self?.navigationItem.title = name
        }
        companyViewModel.value.bind{ [weak self] currentRate in
            self?.lblCurrentRate.text = "Current Share Price: \(currentRate)"
        }

        companyViewModel.onShowError = { [weak self] alert in
            self?.presentSingleButtonDialog(alert: alert)
        }
    }
 
}

// tableview datasource
extension EquityInfoVC: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return certificateViewModel.aryCertificates.value.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch certificateViewModel.aryCertificates.value[indexPath.row]{
        case .normal(let cellValue):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CertificateTableViewCell") as? CertificateTableViewCell else {
                return UITableViewCell()
            }
            cell.configureCell(model: cellValue as CertificateModel)
            return cell
          
        case .error(let message):
            let cell = UITableViewCell()
            cell.isUserInteractionEnabled = false
            cell.textLabel?.text = message
            return cell
        case .empty:
            let cell = UITableViewCell()
            cell.isUserInteractionEnabled = false
            cell.textLabel?.text = "No data available"
            return cell
         
        }
    }
    
}
// tableview delegate
extension EquityInfoVC:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch certificateViewModel.aryCertificates.value[indexPath.row]{
        case .normal(let cellValue):
            var cellModel = cellValue as CertificateModel
            let alertController = UIAlertController(title: "Add number of share for selling", message: "", preferredStyle: .alert)
            alertController.addTextField { (textField : UITextField!) -> Void in
                textField.placeholder = "Number of shares"
                textField.keyboardType = .numberPad
            }
            let saveAction = UIAlertAction(title: "Upload", style: .default, handler: { alert -> Void in
                if let firstTextField = alertController.textFields![0] as UITextField?, firstTextField.text!.count > 0 {
                    if Int(firstTextField.text ?? "0")!  > Int(cellModel.numberOfShares)! {
                    self.showToast(message: "You can only sell share upto \(cellModel.numberOfShares)")
                }
                else {
                        self.certificateViewModel.postCertificateDataForSellingWith(parmas: ["numberOfSharesForSelling":Int(firstTextField.text ?? "0")! as AnyObject,"certificateId":cellModel.certificateId as AnyObject,"companyName":self.companyViewModel.name.value as AnyObject], compliton: { (result) in
                            cellModel.numberOfShareForSelling = firstTextField.text ?? "0"
                            self.certificateViewModel.aryCertificates.value[indexPath.row] = .normal(cellValue: cellModel)
                        })
                    } }
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                (action : UIAlertAction!) -> Void in
                self.tableView.deselectRow(at: indexPath, animated: true)
            })
            
            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)

        case .error( _): break
            
        case .empty: break
            
        }
    }
}

