//
//  ShareObject.swift
//  EquityPlansDemo
//
//  Created by Ashish Patel on 2019/04/07.
//  Copyright © 2019. All rights reserved.
//

import UIKit

class SharedClass: NSObject {
    static let sharedInstance = SharedClass()
    let loader = CustomLoader(title: "")
    var isLocationReceived = false
    func showLoader(strTitle:String) {
        DispatchQueue.main.async {
            self.loader.show(animated: true)
        }
    }
    
    func dismissLoader() {
        DispatchQueue.main.async {
            self.loader.dismiss(animated: true)
        }
    }
    

}
extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String,font: UIFont!) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: font]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String,font: UIFont!) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: font]
        let normal = NSMutableAttributedString(string:text, attributes: attrs)
        append(normal)
        return self
    }
}

