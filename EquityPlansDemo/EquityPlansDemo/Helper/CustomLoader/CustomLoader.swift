//
//  CustomAlert.swift
//  ModalView
//
//  Created by Ashish Patel on 2019/04/07.
//  Copyright © 2019 Ashish. All rights reserved.
//

import UIKit

class CustomLoader : UIView, Loader {
    var backgroundView = UIView()
    var dialogView = UIView()
    var strTitle = ""
    convenience init(title:String) {
        self.init(frame: UIScreen.main.bounds)
        initialize(title: title)
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func initialize(title:String){
        strTitle = title
        dialogView.clipsToBounds = true
        backgroundView.frame = frame
        backgroundView.backgroundColor = UIColor.black
        backgroundView.alpha = 0.6
        addSubview(backgroundView)
        
        let dialogViewWidth = frame.width-100
        
        let jeremyGif = UIImage.gifImageWithName("Loading")
        let imageView = UIImageView(image: jeremyGif)
        
        imageView.contentMode = .scaleAspectFit
        dialogView.addSubview(imageView)
        imageView.startAnimating()
        
        
        let dialogViewHeight = CGFloat(120.0)
        dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
        dialogView.frame.size = CGSize(width: frame.width-100, height: dialogViewHeight)
        imageView.frame = CGRect(x: (dialogView.frame.size.width/2)-43, y: (dialogView.frame.size.height/2)-43, width: 86, height: 86)
        dialogView.backgroundColor = .clear
        addSubview(dialogView)
    }
    @objc func buttonAction(sender: UIButton!) {
        dismiss(animated: true)
    }

    
}
