//
//  UIViewController+Extension.swift
//  EquityPlansDemo
//
//  Created by Ashish Patel on 2019/04/07.
//  Copyright © 2019. All rights reserved.
//

import Foundation
import UIKit


protocol SingleButtonDialogPresenter {
    func presentSingleButtonDialog(alert: SingleButtonAlert)
}


extension UIViewController {
    
    func presentSingleButtonDialog(alert: SingleButtonAlert) {
        let alertController = UIAlertController(title: alert.title,
                                                message: alert.message,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: alert.action.buttonTitle,
                                                style: .default,
                                                handler: { _ in alert.action.handler?() }))
        self.present(alertController, animated: true, completion: nil)
    }
}

extension UIViewController{
    
    func showInternetAlert() {
        
        let okAlert = SingleButtonAlert (
            title: "Could not connect to  network and try again later",
            message: "Failed to update information.",
            action: AlertAction(buttonTitle: "OK", handler: { print("Ok pressed!") })
        )
        
            presentSingleButtonDialog(alert: okAlert)
        
    }
    
    
    func showLocationAlert(){
    
     let locationAlert = SingleButtonAlert (
        title: "Could not find location",
        message: "Enable location for this App. Go to setting page",
        action: AlertAction(buttonTitle: "OK", handler: { print("Ok pressed!") }))
       presentSingleButtonDialog(alert: locationAlert)
    
    }
    
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 20, y: self.view.frame.size.height-100, width: self.view.frame.size.width-40, height: 40))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 5;
        toastLabel.minimumScaleFactor = 0.5
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
}
