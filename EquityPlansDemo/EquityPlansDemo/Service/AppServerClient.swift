//
//  AppServerClient.swift
//  EquityPlansDemo
//
//  Created by Ashish Patel on 2019/04/07.
//  Copyright © 2019. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class AppServerClient {

static func setUrlPathComponent(url:URL,params: [(String, String)])-> URLComponents{
    let queryItems = params.map { URLQueryItem(name: $0.0, value: $0.1) }
    var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)!
    urlComponents.queryItems =   queryItems
    return urlComponents
}

private struct Constants {
    static let baseURL = "http://developerexam.equityplansdemo.com/test/"
}
enum ResourcePath: String {
    case GetListOfCertificates = "sampledata"
    case GetCurrentSharePrice = "fmv"
    case UploadeSellingShare = "https://webhook.site/3843806c-ca88-4872-a337-904932afc0cd"
    var path: String {
        return Constants.baseURL + rawValue
    }
}

    
    
// ALL Certificates
enum CertificatesFailureReason: Int, Error {
    case notFound = 404
}

typealias CertificatesResult = Result<[Certificate], CertificatesFailureReason>
typealias CertificatesResultCompletion = (_ result: CertificatesResult) -> Void

func getListOfCertificates(completion: @escaping CertificatesResultCompletion){
    
    let url = URL(string:ResourcePath.GetListOfCertificates.path)
    print((url?.absoluteString)!)

    Alamofire.request((
        url?.absoluteString)!, method:.get, parameters:nil, encoding: JSONEncoding.default)
        .validate(statusCode: 200 ..< 300)
        .responseJSON{ response in
            switch response.result {
            case .success:
                guard let json = response.result.value, let arySwiftyJSON = JSON(rawValue: json)?.arrayValue else {
                    completion(.failure(nil))
                    return
                }
                let aryCertificates = arySwiftyJSON.compactMap {(data) -> Certificate? in
                    return Certificate(JSON: data.rawValue as! [String : Any])
                }
                completion(.success(payload:aryCertificates))
            case .failure(_):
                if let statusCode = response.response?.statusCode,
                    let reason = CertificatesFailureReason(rawValue: statusCode) {
                    completion(.failure(reason))
                }
                completion(.failure(nil))
            }
    }
}

// Current Company
enum CompanyFailureReason: Int, Error {
    case notFound = 404
}

typealias CompanyResult = Result<Company, CompanyFailureReason>
typealias CompanyResultCompletion = (_ result: CompanyResult) -> Void



func getCompanyData(completion: @escaping CompanyResultCompletion){
    let url = URL(string:ResourcePath.GetCurrentSharePrice.path)
    print((url?.absoluteString)!)
    
    Alamofire.request((
        url?.absoluteString)!, method:.get, parameters:nil, encoding: JSONEncoding.default)
        .validate(statusCode: 200 ..< 300)
        .responseJSON { response in
            
            switch response.result {
            case .success:
                guard let json = response.result.value, let swiftyJSON = JSON(rawValue: json),
                    let currentCompany = Company(json: swiftyJSON) else {
                        completion(.failure(nil))
                        return
                }
                completion(.success(payload:currentCompany))
            case .failure(_):
                if let statusCode = response.response?.statusCode,
                    let reason = CompanyFailureReason(rawValue: statusCode) {
                    completion(.failure(reason))
                }
                completion(.failure(nil))
            }
    }
}
    
    // Sell Certificates
    
    typealias SellCertificatesResult = Result<JSON, CompanyFailureReason>
    typealias SellCertificatesResultCompletion = (_ result: SellCertificatesResult) -> Void
    
    func postListOfSellingCertificates(params:[String : AnyObject]?,completion: @escaping SellCertificatesResultCompletion){
        
        let url = URL(string:ResourcePath.UploadeSellingShare.rawValue)
        print((url?.absoluteString)!)
        
        Alamofire.request((url?.absoluteString)!, method: .post, parameters: params, encoding: JSONEncoding.default, headers: nil)
            .validate(statusCode: 200 ..< 300)
            .responseJSON{ response in
                debugPrint(#line)
                debugPrint(#function)
                if response.result.isSuccess {
                    let resJson = JSON(response.result.value!)
                    completion(.success(payload:resJson))
                }
                if response.result.isFailure {
                    if let statusCode = response.response?.statusCode,
                        let reason = CompanyFailureReason(rawValue: statusCode) {
                        completion(.failure(reason))
                    }
                    completion(.failure(nil))
        }
    }
    }

}
